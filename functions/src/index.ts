import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
import * as firebaseHelper from 'firebase-functions-helper/dist';
import * as express from 'express';
import * as bodyParser from "body-parser";
admin.initializeApp(functions.config().firebase);
const db = admin.firestore();
const app = express();
const main = express();
const meetingsCollection = 'meetings';
const meetingRecapCollection='meeting_recaps';
// const meetingRecapsCollection="meeting_recaps";
main.use('/api/v1', app);
main.use(bodyParser.json());
main.use(bodyParser.urlencoded({ extended: false }));
// webApi is your functions name, and you will pass main as 
// a parameter
export const webApi = functions.https.onRequest(main);

interface Meeting{  
    date_end: Date
    date_start: Date
    email: String
    hostname: String
    intervals: Number
    passcode: String
    room_id: String
    title:  String
    description: String
}
interface MeetingRecap{
    classification_0:Number
    classification_1:Number
    classification_2:Number
    classification_3:Number
    classification_4:Number
    classification_5:Number
    classification_6:Number
    classification_7:Number
    meeting_id:String
    timestamp:Date
}

// index
app.get('/', (req, res) => {
    res.status(200).send("Heloo!, Welcome to Zmood");
})
//recap
app.get('/recap/:meeting_id',(req,res)=>{
    firebaseHelper.firestoreHelper
    .getDocument(db,meetingRecapCollection,req.params.meeting_id)
    .then(doc=>res.status(200).send(doc))
    .catch(err=>res.status(400).send(`error : ${err}`));
})
//startMeeting
app.post('/startMeeting',async (req,res)=>{
    try {
        const timeNowJakarta= new Date();
        const data: Meeting = {
            date_end: new Date(timeNowJakarta),
            date_start: new Date(timeNowJakarta),
            email: req.body['email'],
            hostname: req.body['hostname'],
            intervals: req.body['intervals'],
            passcode: req.body['passcode'],
            room_id: req.body['room_id'],
            title: req.body['title'],
            description: req.body['description']
        }
        const newDoc = await firebaseHelper.firestoreHelper
        .createNewDocument(db,meetingsCollection,data);
        res.status(201).send(`new data: ${newDoc.id}`);
    } catch (err) {
        res.status(400).send(`error : ${err}`);
    }        

})

//addRecap
app.post('/recap/:meeting_id',async (req,res)=>{
    try{
        const timeNowJakarta = new Date()
        const data:MeetingRecap ={
            classification_0:req.body['0'],
            classification_1:req.body['1'],
            classification_2:req.body['2'],
            classification_3:req.body['3'],
            classification_4:req.body['4'],
            classification_5:req.body['5'],
            classification_6:req.body['6'],
            classification_7:req.body['7'],
            meeting_id:req.params.meeting_id,
            timestamp:new Date(timeNowJakarta)
        }
        const insertTask= await firebaseHelper.firestoreHelper.createNewDocument(db,meetingRecapCollection,data);
        if(insertTask){
            res.status(200).send(`Success insert : ${insertTask.id}`)
        }else{
            res.status(400).send(`failure`)
        }
    }catch(err){
        res.status(400).send(`errp`)
    }
});

//stopMeeting
app.patch('/stopMeeting/:meeting_id',async (req,res)=>{
    try{
        const timeNowJakarta= new Date();
        const data={"date_end":new Date(timeNowJakarta)};
        const updateTask = await firebaseHelper.firestoreHelper.updateDocument(db,meetingsCollection,req.params.meeting_id,data);
        res.status(200).send(updateTask);
    }catch(err){
        res.status(400).send(`error : ${err}`);
    }
})